@extends('layouts.app')
@section('title') Crear un nuevo articulo @endsection
@section('content')


    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Crear un nuevo articulo</div>
                    <div class="panel-body">
                        <div class="col-md-10 col-md-offset-1">
                          {{ Form::open(array('route' => 'articles.store','files'=>'true','method'=> 'POST')) }}
                        <div class="form-group">
                          {{-- nombre del articulo --}}
                          {!! Form::label('name', 'Nombre del articulo') !!}
                          {!!Form::text('name','',['class' => 'form-control'])!!}
                        </div>
                        <div class="form-group">
                          {{-- marca del aritculo --}}
                          {!! Form::label('branch', 'Marca del articulo') !!}
                          {!!Form::text('branch','',['class' => 'form-control','required'])!!}
                        </div>
                        <div class="form-group">
                          {{-- modelo del arituclo --}}
                          {!! Form::label('model', 'Modelo del articulo') !!}
                          {!!Form::text('model','',['class' => 'form-control','required'])!!}
                        </div>
                        <div class="form-group">
                          {{-- precio del articulo --}}
                          {!! Form::label('price', 'Precio del articulo ej: 10.000') !!}
                          {!!Form::number('price','',['class' => 'form-control','required'])!!}
                        </div>
                        <div class="form-group">
                          {{-- precio del articulo --}}
                          {{Form::label('image', 'Imagen del producto')}}
                          {{Form::text('image','',array('class'=>'form-control','readonly'))}}
                          {{Form::file('image')}}
                        </div>
                          {{Form::hidden('user_id',Auth::user()->id)}}
                        <div class="form-group">
                          {!! Form::submit("REGISTRAR", ['class' => 'btn btn-success','required']) !!}
                        </div>
                        {{ Form::close() }}
                      </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
