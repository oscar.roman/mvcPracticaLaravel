@extends('layouts.app')
  @section('title','Editar un Articulo')
@section('content')

  <h1 class="text-center text-info">Este es el index de articles</h1>


    <div class="container">
      {{ Form::open(array('url' => 'foo/bar', 'files'=>'true')) }}
      <div class="form-group form-group-lg label-floating">
        {{Form::label('name', 'Nombre del producto',array('class'=>'control-label'))}}
        {{Form::text('name','',array('class'=>'form-control'))}}
      </div>
      <div class="form-group form-group-lg label-floating">
        {{Form::label('branch', 'Marca del producto',array('class'=>'control-label'))}}
        {{Form::text('branch','',array('class'=>'form-control'))}}
      </div>
      <div class="form-group form-group-lg label-floating">
        {{Form::label('model', 'Modelo del producto',array('class'=>'control-label'))}}
        {{Form::text('model','',array('class'=>'form-control'))}}
      </div>
      <div class="form-group form-group-lg label-floating">
        {{Form::label('price', 'Precio del producto',array('class'=>'control-label'))}}
        {{Form::text('price','',array('class'=>'form-control'))}}
      </div>
      <div class="form-group form-group-lg ">
        {{Form::label('image', 'Imagen del producto')}}
        {{Form::text('','',array('class'=>'form-control','readonly'))}}
        {{Form::file('image')}}
      </div>

      {{ Form::close() }}
    </div>



@endsection
