<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
  protected $table = 'purchases';
  protected $fillable = ['direction','user_id'];

  public function user(){
    return $this->belongsTo('App\User');
  }
  public function orders(){
    return $this->hasMany('App\Order');
  }
}
