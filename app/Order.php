<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
  protected $table = 'orders';
  protected $fillable = ['amount','article_id','purchase_id'];

  public function article(){
    return $this->belongsTo('App\Article');
  }
  public function purchase(){
    return $this->belongsTo('App\Purchase');
  }
}
